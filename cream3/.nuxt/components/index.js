export { default as Author } from '../..\\components\\author.vue'
export { default as Blocked } from '../..\\components\\Blocked.vue'
export { default as Carousel } from '../..\\components\\Carousel.vue'
export { default as Cv } from '../..\\components\\Cv.vue'
export { default as Footbar } from '../..\\components\\Footbar.vue'
export { default as Loading } from '../..\\components\\loading.vue'
export { default as Map } from '../..\\components\\Map.vue'
export { default as Navigation } from '../..\\components\\Navigation.vue'
export { default as Orbit } from '../..\\components\\Orbit.vue'
export { default as Pillow } from '../..\\components\\Pillow.vue'
export { default as Players } from '../..\\components\\Players.vue'
export { default as QMessage } from '../..\\components\\QMessage.vue'
export { default as Qrlinks } from '../..\\components\\Qrlinks.vue'
export { default as Social } from '../..\\components\\Social.vue'
export { default as Transitions } from '../..\\components\\transitions.vue'
export { default as Visited } from '../..\\components\\Visited.vue'
export { default as VisitedGlobe } from '../..\\components\\VisitedGlobe.vue'
export { default as Audio } from '../..\\components\\Player\\audio.js'

export const LazyAuthor = import('../..\\components\\author.vue' /* webpackChunkName: "components_author" */).then(c => c.default || c)
export const LazyBlocked = import('../..\\components\\Blocked.vue' /* webpackChunkName: "components_Blocked" */).then(c => c.default || c)
export const LazyCarousel = import('../..\\components\\Carousel.vue' /* webpackChunkName: "components_Carousel" */).then(c => c.default || c)
export const LazyCv = import('../..\\components\\Cv.vue' /* webpackChunkName: "components_Cv" */).then(c => c.default || c)
export const LazyFootbar = import('../..\\components\\Footbar.vue' /* webpackChunkName: "components_Footbar" */).then(c => c.default || c)
export const LazyLoading = import('../..\\components\\loading.vue' /* webpackChunkName: "components_loading" */).then(c => c.default || c)
export const LazyMap = import('../..\\components\\Map.vue' /* webpackChunkName: "components_Map" */).then(c => c.default || c)
export const LazyNavigation = import('../..\\components\\Navigation.vue' /* webpackChunkName: "components_Navigation" */).then(c => c.default || c)
export const LazyOrbit = import('../..\\components\\Orbit.vue' /* webpackChunkName: "components_Orbit" */).then(c => c.default || c)
export const LazyPillow = import('../..\\components\\Pillow.vue' /* webpackChunkName: "components_Pillow" */).then(c => c.default || c)
export const LazyPlayers = import('../..\\components\\Players.vue' /* webpackChunkName: "components_Players" */).then(c => c.default || c)
export const LazyQMessage = import('../..\\components\\QMessage.vue' /* webpackChunkName: "components_QMessage" */).then(c => c.default || c)
export const LazyQrlinks = import('../..\\components\\Qrlinks.vue' /* webpackChunkName: "components_Qrlinks" */).then(c => c.default || c)
export const LazySocial = import('../..\\components\\Social.vue' /* webpackChunkName: "components_Social" */).then(c => c.default || c)
export const LazyTransitions = import('../..\\components\\transitions.vue' /* webpackChunkName: "components_transitions" */).then(c => c.default || c)
export const LazyVisited = import('../..\\components\\Visited.vue' /* webpackChunkName: "components_Visited" */).then(c => c.default || c)
export const LazyVisitedGlobe = import('../..\\components\\VisitedGlobe.vue' /* webpackChunkName: "components_VisitedGlobe" */).then(c => c.default || c)
export const LazyAudio = import('../..\\components\\Player\\audio.js' /* webpackChunkName: "components_Player/audio" */).then(c => c.default || c)
