import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from '@nuxt/ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _51950314 = () => interopDefault(import('..\\pages\\aboutme.vue' /* webpackChunkName: "pages/aboutme" */))
const _f03bbd7a = () => interopDefault(import('..\\pages\\category\\index.vue' /* webpackChunkName: "pages/category/index" */))
const _2cbfe062 = () => interopDefault(import('..\\pages\\contact.vue' /* webpackChunkName: "pages/contact" */))
const _97c3ad7e = () => interopDefault(import('..\\pages\\category\\devinfo-credits.vue' /* webpackChunkName: "pages/category/devinfo-credits" */))
const _7b854592 = () => interopDefault(import('..\\pages\\category\\ex.vue' /* webpackChunkName: "pages/category/ex" */))
const _6175f5d4 = () => interopDefault(import('..\\pages\\category\\music.vue' /* webpackChunkName: "pages/category/music" */))
const _b4c7cce2 = () => interopDefault(import('..\\pages\\category\\savecv.vue' /* webpackChunkName: "pages/category/savecv" */))
const _731ef0f9 = () => interopDefault(import('..\\pages\\category\\travel.vue' /* webpackChunkName: "pages/category/travel" */))
const _565e17b2 = () => interopDefault(import('..\\pages\\category\\_slug\\index.vue' /* webpackChunkName: "pages/category/_slug/index" */))
const _48dfff17 = () => interopDefault(import('..\\pages\\category\\_slug\\articles\\_.vue' /* webpackChunkName: "pages/category/_slug/articles/_" */))
const _542bc541 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/aboutme",
    component: _51950314,
    name: "aboutme"
  }, {
    path: "/category",
    component: _f03bbd7a,
    name: "category"
  }, {
    path: "/contact",
    component: _2cbfe062,
    name: "contact"
  }, {
    path: "/category/devinfo-credits",
    component: _97c3ad7e,
    name: "category-devinfo-credits"
  }, {
    path: "/category/ex",
    component: _7b854592,
    name: "category-ex"
  }, {
    path: "/category/music",
    component: _6175f5d4,
    name: "category-music"
  }, {
    path: "/category/savecv",
    component: _b4c7cce2,
    name: "category-savecv"
  }, {
    path: "/category/travel",
    component: _731ef0f9,
    name: "category-travel"
  }, {
    path: "/category/:slug",
    component: _565e17b2,
    name: "category-slug"
  }, {
    path: "/category/:slug/articles/*",
    component: _48dfff17,
    name: "category-slug-articles-all"
  }, {
    path: "/",
    component: _542bc541,
    name: "index"
  }],

  fallback: false
}

function decodeObj(obj) {
  for (const key in obj) {
    if (typeof obj[key] === 'string') {
      obj[key] = decode(obj[key])
    }
  }
}

export function createRouter () {
  const router = new Router(routerOptions)

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    const r = resolve(to, current, append)
    if (r && r.resolved && r.resolved.query) {
      decodeObj(r.resolved.query)
    }
    return r
  }

  return router
}
