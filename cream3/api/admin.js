const httpProxy = require('http-proxy')
const proxy = httpProxy.createProxyServer()
const PORT = process.env.PORT
var NODE_PORT = 8000;
var DJANGO_PORT = 7777;

export default function(req, res, next) {
  if(req.url.indexOf('/admin') === 0) {
        // Depending on your application structure you can proxy to a node application running on another port, or serve content directly here
        proxy.web(req, res, { target: 'http://localhost:' + DJANGO_PORT });

        // Proxy WebSocket requests if needed
        proxy.on('upgrade', function(req, socket, head) {
            proxy.ws(req, socket, head, { target: 'ws://localhost:' + DJANGO_PORT });
        });
    } else {
        proxy.web(req, res, { target: 'http://localhost:' + NODE_PORT });
    }
    next()
}
