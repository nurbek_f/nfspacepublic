import html2Canvas from 'html2canvas';
import JsPDF from 'jspdf';

export default {
  install(Vue, options) {
    Vue.prototype.getPdf = function (file, title, isShowPreviewFullTime) {
      // var paragraph = document.querySelector('#hugeblock');
      // var mediaQueryList = window.matchMedia('(max-width: 920px)');
      // var jk = document.styleSheets[13];
      // var mediaRule = document.styleSheets[13].cssRules[0];
      // console.log(jk);
      // var initialRule = mediaRule.cssRules[0];
      // jk.deleteRule(jk.cssRules[0])
      // console.log(mediaRule);

      //  if(document.getElementsByTagName('meta')['viewport'].content=='width= 1440px'){
      //   document.getElementsByTagName('meta')['viewport'].content='width= 400px';
      //  }else{
      //   document.getElementsByTagName('meta')['viewport'].content='width= 1440px';
      // }
      // document.getElementsByTagName('meta')['viewport'].content='width=1440px;';

      html2Canvas(document.querySelector(file), {
      }).then(function (canvas) {
          let pageData = canvas.toDataURL('image/jpeg', 1.0)
          let PDF = new JsPDF('', 'pt', [canvas.width, canvas.height])
          PDF.addImage(pageData, 'JPEG', 0, 0, canvas.width, canvas.height)
          PDF.save(title + '.pdf')
        }
      );
      // mediaRule.insertRule(initialRule);
        // console.log(mediaRule);
    },
      Vue.prototype.printDiv = function (file, title, isShowPreviewFullTime) {
        var acolcolor = document.getElementById(11).style.backgroundColor
        var bcolcolor = document.getElementById(22).style.backgroundColor
        var ccolcolor = document.getElementById("workhist").style.backgroundColor
        var cccolcolor = document.getElementById("workhistwrapper").style.backgroundColor
        var dcolcolor = document.getElementById("skillz").style.backgroundColor
        document.getElementById(11).style.backgroundColor = "white"
        document.getElementById(22).style.backgroundColor = "white"
        document.getElementById("workhist").style.backgroundColor = "white"
        document.getElementById("skillz").style.backgroundColor = "white"
        document.getElementById("workhistwrapper").style.backgroundColor = "white"
        html2Canvas(document.querySelector(file), {
        }).then(function (canvas) {
            let pageData = canvas.toDataURL('image/jpeg', 1.0)
            let PDF = new JsPDF('', 'pt', [canvas.width, canvas.height])
            PDF.addImage(pageData, 'JPEG', 0, 0, canvas.width, canvas.height)
            PDF.save(title + '.pdf')

          }
        );
        document.getElementById(11).style.backgroundColor = acolcolor
        document.getElementById(22).style.backgroundColor = bcolcolor
        document.getElementById("workhistwrapper").style.backgroundColor = cccolcolor
        document.getElementById("workhist").style.backgroundColor = ccolcolor
        document.getElementById("skillz").style.backgroundColor = dcolcolor
      }
  }
}
