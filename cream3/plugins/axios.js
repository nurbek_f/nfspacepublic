export default function({ $axios, store }) {
    $axios.onError(error => {
        store.dispatch('setError', error);
    });
}
