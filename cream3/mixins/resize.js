var maxWidth  = this.$refs["outer"].width();
var maxHeight = this.$refs["outer"].height();

var windowWidth = $(window).width();
var windowHeight = $(window).height();

$(window).resize(function(evt) {

    var width = window.width();
    var height = window.height();
    var scale;

    // early exit
    if(width >= windowWidth && height >= windowHeight) {
        this.$refs["outer"].css({'-webkit-transform': ''});
        this.$refs["wrap"].css({ width: '', height: '' });
        return;
    }

    //scale = Math.min(width/maxWidth, height/maxHeight);
    scale = Math.min(width/windowWidth, height/windowHeight);

    this.$refs["outer"].css({'-webkit-transform': 'scale(' + scale + ')'});
    this.$refs["outer"].css({ width: maxWidth * scale, height: maxHeight * scale });
});
