export default {
  server: {
    port: process.env.PORT || 8000,
    host: '0.0.0.0',
  },

  publicRuntimeConfig: {
    baseURL: '${PUBLIC_URL}${BASE_URL}',
    API_TOKEN: process.env.API_TOKEN
  },
  privateRuntimeConfig: {
    baseURL: '${PUBLIC_URL}${BASE_URL}',
    API_TOKEN: process.env.API_TOKEN
  },

  target: 'server',


  head: {
    title: 'nfspace.me',
    script: [
      {

      }
    ],
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, user-scalable=no' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },


  css: [
    'vuesax/dist/vuesax.css',
    'boxicons/css/boxicons.css',
    '~/assets/main.css',
    'highlight.js/styles/atom-one-dark.css'
  ],
  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
    '@/plugins/vuesax',
    '@/plugins/filters',
    "@/plugins/vee-validate.js",
    {src: '@/plugins/vue-scroll',ssr: false,},
    { src: '~/plugins/vue-html-pdf.js', ssr: false },

    // {src: '@/plugins/vue-awesome-swiper', ssr:false},
  ],

  components: true,

  buildModules: [
    '@nuxtjs/color-mode'
  ],

  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/auth',
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/proxy',
    '@nuxtjs/sitemap',
    '@nuxtjs/webpackmonitor',
  ],

  sitemap: {
    exclude: [
     '/api',
     '/admin/**',
     '/category/savecv',

   ],
  },

  colorMode: {
    preference: 'dark', // default value of $colorMode.preference
    fallback: 'dark', // fallback value if not system preference found
    // hid: 'nuxt-color-mode-script',
    // globalName: '__NUXT_COLOR_MODE__',
    // componentName: 'ColorScheme',
    // cookie: {
    //   key: 'nuxt-color-mode',
    //   options: {
    //     path: nuxt.options.router.base // https://nuxtjs.org/api/configuration-router#base
    //   }
    // }
  },



  axios: {
    baseURL: process.env.BACKEND_URL,


  },
  // serverMiddleware: [
  //   {
  //     path: '/admin/',
  //     handler: '~/api/admin.js'
  //   }
  // ],

  proxy: {
  '/backend/': { target: `http://localhost:${process.env.DJANGO_PORT}/`, pathRewrite: {'^/backend/': ''}, changeOrigin: true },
    '/admin/': { target: `http://localhost:${process.env.DJANGO_PORT}/`, pathRewrite: {'^/backend/admin/': ''}, changeOrigin: true },
      '/api/': { target: `http://localhost:${process.env.DJANGO_PORT}/`, pathRewrite: {'^/backend/api/': ''}, changeOrigin: true },
        '/api/categories/': { target: `http://localhost:${process.env.DJANGO_PORT}/`, pathRewrite: {'^/backend/api/categories/': ''}, changeOrigin: true }
},


    // auth: {
    //   // username: 'client999',
    //   // password: 'diodesandtriods999$',
    //   Authorization: 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjE0ODg0MjYyLCJqdGkiOiJmODdlYjU2NThiOGQ0ZTk3YjYyYzQ1NzRmZGM0MzE5YyIsInVzZXJfaWQiOjJ9.3GNPZ1xdjLi2fAZHxbI76awBX8GlxmQ_AsRLExD7Frc'
    // },
    // https: true,
    // headers: {
    //
    // }
    // }
    // proxy: true,
    // withCredentials: true,
//   auth: {
//   strategies: {
//     local: {
//       endpoints: {
//         login: { url: '/api-auth/login', method: 'post', propertyName: 'token' },
//         logout: { url: '/api/auth/logout', method: 'post' },
//
//       },
//       tokenRequired: false,
//       tokenType: false
//     },
//     cookie: {
//       prefix: 'auth.',
//       options: {
//         path: '/'
//       }
//     }
//   }
// },
  // router: {
  //   middleware: ['auth']
  // },

  // proxy: {
  //   '/api/': {target: 'http://localhost:7777/', pathRewrite: {'^/api/': ''}}
  // },
  // env: {
  //     baseUrl: process.env.BASE_URL || 'http://localhost:7777'
  //   },
  build: {
    transpile: ["vee-validate/dist/rules"],
    extend(config, ctx){
      config.module.rules.push({
        test: /\.(ogg|mp3|wav|mpe?g|pdf)$/i,
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]'
        }
      })
    }
  }
}
