from django.db import models
from PIL import Image
from cloudinary_storage.storage import RawMediaCloudinaryStorage



class Categories(models.Model):
    def upload_to_sorter(instance, filename):
        return "image/article/{}/{}".format(instance.slug, filename)

    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to=upload_to_sorter, null=True, blank=True)
    tags = models.CharField(max_length=200)
    slug = models.SlugField(null=True)

    class Meta:
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.title


class Article(models.Model):
    def upload_to_sorter(instance, filename):
        return "image/{}/{}/{}".format(instance.category.slug, instance.slug, filename)

    category = models.ForeignKey(Categories, default=1, related_name='category', verbose_name="Category", on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    subtitle = models.CharField(max_length=255)
    image = models.ImageField(upload_to=upload_to_sorter)
    text = models.TextField()
    published = models.DateField(auto_now=True)

    slug = models.SlugField(null=True)

    class Meta:
        verbose_name_plural = "Articles"

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("home:article", kwargs={'slug': self.slug})


class Visited(models.Model):
    country = models.CharField(max_length=255)
    city = models.CharField(max_length=255)

    def __str__(self):
        return self.city


class VisitedPictures(models.Model):
    place = models.ForeignKey(Visited, default=None, on_delete=models.CASCADE)
    pics = models.FileField(upload_to="visitedcities/", storage=RawMediaCloudinaryStorage())
    pics_thumbnail = models.FileField(upload_to="visitedcities_thumbnail/", null=True, blank=True, storage=RawMediaCloudinaryStorage())
    descrip = models.CharField(max_length=500)
    date = models.DateField()

    def __str__(self):
        return self.place.city

    def save(self, *args, **kwargs):
        super(VisitedPictures, self).save(*args, **kwargs)
        with Image.open(self.pics.path) as im:
            if im.width > 400 or im.height> 300:
                output_size = (400, 300)
                im.thumbnail((400, 300))
                im.save(self.pics_thumbnail.path, compression="jpeg", quality=40)


class Music(models.Model):
    title = models.CharField(max_length=40)
    file = models.FileField(upload_to="musicfiles/", storage=RawMediaCloudinaryStorage())
    genre = models.CharField(max_length=200)


class UserMessages(models.Model):
    msg_title = models.CharField(max_length=200)
    msg_sender = models.CharField(max_length=80)
    msg_sender_email = models.EmailField(max_length=200)
    msg = models.TextField()
    msg_sent = models.DateTimeField()


class CV(models.Model):
    name = models.CharField(max_length=20)
    surname = models.CharField(max_length=20)

    class Meta:
        verbose_name_plural = "CVs"

class Links(models.Model):
    parent = models.ForeignKey(CV, related_name='links', verbose_name="Parent_workplace", on_delete=models.CASCADE )
    icon_name = models.CharField(max_length=15)
    link = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = "Links"

class ProfessionalSummary(models.Model):
    parent = models.ForeignKey(CV, related_name='summary', on_delete=models.CASCADE )
    summary_text = models.TextField()

    class Meta:
        verbose_name_plural = "ProfessionalSummaries"

class WorkExperienceResponsib(models.Model):
    parent = models.ForeignKey(CV, related_name='experience_resp', on_delete=models.CASCADE, null=True,)
    responsibilities = models.CharField(max_length=200, blank=True)


class WorkExperience(models.Model):
    parent = models.ForeignKey(CV, related_name='experience', on_delete=models.CASCADE)
    workplace_position = models.CharField(max_length=200)
    fromdate = models.CharField(max_length=10)
    todate = models.CharField(max_length=10)
    specificroute = models.CharField(max_length=200, blank=True)
    responsibilities = models.ManyToManyField(WorkExperienceResponsib)


class Skills(models.Model):
    parent = models.ForeignKey(CV, related_name='skills', on_delete=models.CASCADE )
    main_skill = models.CharField(max_length=200)
    skill_details = models.CharField(max_length=200, blank=True)

    class Meta:
        verbose_name_plural = "Skills"


class QRCode(models.Model):
    parent = models.ForeignKey(CV, related_name='qr', on_delete=models.CASCADE )
    qr = models.FileField(upload_to="qr/", storage=RawMediaCloudinaryStorage())
