from django.contrib import admin
from django.utils.html import format_html
from .models import *

from markdownx.widgets import AdminMarkdownxWidget
# from trumbowyg.widgets import TrumbowygWidget


class CategoriesA(admin.ModelAdmin):
    fields = ('title', 'image', 'tags', 'slug')
    def get_list_display(self, request):
        return [field.name for field in self.model._meta.concrete_fields if field.name != "id"]

class ArticleA(admin.ModelAdmin):
    fields = ('category','title','subtitle', 'text', 'image', 'slug')
    formfield_overrides = {
        models.TextField: {'widget': AdminMarkdownxWidget},
    }
    def get_list_display(self, request):
        return [field.name for field in self.model._meta.concrete_fields if field.name != "id"]


class VisitedA(admin.ModelAdmin):
    fields = ('city', 'country',)
    list_display = ('city', 'country')


class VisitedPicsA(admin.ModelAdmin):
    fields = ('place', 'pics', 'descrip', 'date','pics_thumbnail')
    list_display = ("place", 'descrip', 'pics_thumbnail', 'date')

    def thumbnail(self, obj):
        return format_html('<img src="{}" style="max-width: 150px; \
                           "/>'.format(obj.pics.url))


class MusicA(admin.ModelAdmin):
    fields = ('title', 'file', 'genre')
    list_display = ('title', 'genre')


class UserMessagesA(admin.ModelAdmin):
    fields = ('msg_title', 'msg_sender', 'msg_sender_email', 'msg', 'msg_sent')
    list_display = ('msg_sender', 'msg_title', 'msg_sender_email', 'msg_sent')


class CVA(admin.ModelAdmin):
    fields = ('name', 'surname',)
    list_display = ('name', 'surname',)


class LinksA(admin.ModelAdmin):
    fields = ('icon_name', 'link', 'parent')
    list_display = ('icon_name', 'link', 'parent')


class ProfessionalSummaryA(admin.ModelAdmin):
    fields = ('summary_text', 'parent')
    list_display = ('summary_text','parent',)
    formfield_overrides = {
        models.TextField: {'widget': AdminMarkdownxWidget},
    }


class WorkExperienceA(admin.ModelAdmin):
    fields = ('workplace_position', 'fromdate', 'todate', 'specificroute', 'responsibilities', 'parent')
    list_display = ('workplace_position', 'fromdate', 'todate', 'specificroute', 'parent')


class WorkExperienceResponsibA(admin.ModelAdmin):
    fields = ('responsibilities', 'parent')



class SkillsA(admin.ModelAdmin):
    fields = ('main_skill', 'skill_details', 'parent')
    list_display = ('main_skill', 'skill_details','parent')

class QRCodeA(admin.ModelAdmin):
    fields = ('qr','parent')
    list_display = ('qr','parent')


admin.site.register(CV, CVA)
admin.site.register(Links, LinksA)
admin.site.register(ProfessionalSummary, ProfessionalSummaryA)
admin.site.register(WorkExperience, WorkExperienceA)
admin.site.register(WorkExperienceResponsib, WorkExperienceResponsibA)
admin.site.register(Skills, SkillsA)
admin.site.register(QRCode, QRCodeA)

admin.site.register(Categories, CategoriesA)
admin.site.register(Article, ArticleA)
admin.site.register(Visited, VisitedA)
admin.site.register(VisitedPictures, VisitedPicsA)
admin.site.register(UserMessages, UserMessagesA)
admin.site.register(Music, MusicA)
