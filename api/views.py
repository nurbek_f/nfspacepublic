from django.shortcuts import render
from .models import Article, Categories, Visited, VisitedPictures, Music, UserMessages
from .models import CV, Links, ProfessionalSummary, WorkExperience,  WorkExperienceResponsib, Skills, QRCode
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import CategoriesSer, ArticleSer, VisitedSer, VisitedPicsSer, MusicSer,UserMessagesSer
from .serializers import  CVSer, LinksSer, ProfessionalSummarySer, WorkExperienceSer,  WorkExperienceResponsibSer, SkillsSer, QRCodeSer
from django.utils.safestring import SafeString


class CategoriesV(viewsets.ModelViewSet):
    queryset = Categories.objects.all()
    serializer_class = CategoriesSer
    permission_classes = [permissions.IsAuthenticated]


class ArticleV(viewsets.ModelViewSet):
    # queryset = Article.objects.all()
    serializer_class = ArticleSer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        queryset = Article.objects.all()
        slug = self.request.query_params.get('slug', None)
        if slug is not None:
            queryset = queryset.filter(slug=slug)
        return queryset


class VisitedV(viewsets.ModelViewSet):
    queryset = Visited.objects.all()
    serializer_class = VisitedSer
    permission_classes = [permissions.IsAuthenticated]


class VisitedPicsV(viewsets.ModelViewSet):
    serializer_class = VisitedPicsSer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        queryset = VisitedPictures.objects.all()
        city = self.request.query_params.get('city', None)
        country = self.request.query_params.get('country', None)
        if city is not None:
            queryset = queryset.filter(place__city=city)
        return queryset


class MusicV(viewsets.ModelViewSet):
    queryset = Music.objects.all()
    serializer_class = MusicSer
    permission_classes = [permissions.IsAuthenticated]



class UserMessagesV(APIView):
    queryset = UserMessages.objects.all()
    serializer_class = UserMessagesSer
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, format="None"):
        data = {}
        data['Mystic'] = 'I can only eat :('
        return Response(data=data)

    def post(self, request, format="None"):
        serializer = UserMessagesSer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CVV(viewsets.ModelViewSet):
    queryset = CV.objects.all()
    serializer_class = CVSer
    permission_classes = [permissions.IsAuthenticated]

class LinksV(viewsets.ModelViewSet):
    queryset = Links.objects.all()
    serializer_class = LinksSer
    permission_classes = [permissions.IsAuthenticated]

class ProfessionalSummaryV(viewsets.ModelViewSet):
    queryset = CV.objects.all()
    serializer_class = ProfessionalSummarySer
    permission_classes = [permissions.IsAuthenticated]

class WorkExperienceV(viewsets.ModelViewSet):
    queryset = WorkExperience.objects.all()
    serializer_class = WorkExperienceSer
    permission_classes = [permissions.IsAuthenticated]

class WorkExperienceResponsibV(viewsets.ModelViewSet):
    queryset = WorkExperienceResponsib.objects.all()
    serializer_class = WorkExperienceResponsibSer
    permission_classes = [permissions.IsAuthenticated]

class SkillsSer(viewsets.ModelViewSet):
    queryset = Skills.objects.all()
    serializer_class = SkillsSer
    permission_classes = [permissions.IsAuthenticated]

class QRCodeSer(viewsets.ModelViewSet):
    queryset = QRCode.objects.all()
    serializer_class = QRCodeSer
    permission_classes = [permissions.IsAuthenticated]
