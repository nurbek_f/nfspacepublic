from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from .views import CategoriesV, ArticleV, VisitedV, VisitedPicsV, MusicV, UserMessagesV
from .views import CVV, LinksV, ProfessionalSummaryV, WorkExperienceV, WorkExperienceResponsibV, SkillsSer, QRCodeSer
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

router = routers.DefaultRouter()
router.register(r'article', ArticleV, basename="article/")
router.register(r'categories', CategoriesV)
router.register(r'visitedplaces', VisitedV)
router.register(r'visitedplacespics', VisitedPicsV, basename="visitedplaces/")
router.register(r'music', MusicV)
router.register(r'cv', CVV)



urlpatterns = [
    path('api/', include(router.urls)),
    path('api/usermessage/',UserMessagesV.as_view()),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/token/', TokenObtainPairView.as_view()),
    path('api/token/refresh', TokenRefreshView.as_view()),


]
