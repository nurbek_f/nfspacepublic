from .models import Article, Categories, Visited, VisitedPictures, Music, UserMessages
from .models import CV, Links, ProfessionalSummary, WorkExperience,  WorkExperienceResponsib, Skills, QRCode
from rest_framework import serializers

class CategoriesSer(serializers.ModelSerializer):
    class Meta:
        model = Categories
        fields = ['title', 'image', 'tags', 'slug']


class ArticleSer(serializers.ModelSerializer):
    category = CategoriesSer(many=False, read_only=True)
    class Meta:
        model = Article
        fields = ['category','title', 'subtitle', 'text', 'image', 'published', 'slug',]


class VisitedSer(serializers.ModelSerializer):
    class Meta:
        model = Visited
        fields = ['country', 'city']


class VisitedPicsSer(serializers.ModelSerializer):
    place = VisitedSer(many=False, read_only=True)
    class Meta:
        model = VisitedPictures
        fields = ['place', 'pics','pics_thumbnail', 'descrip', 'date']


class MusicSer(serializers.ModelSerializer):
    class Meta:
        model = Music
        fields = ['title', 'file', 'genre']


class UserMessagesSer(serializers.ModelSerializer):
    class Meta:
        model = UserMessages
        fields = ['msg_title', 'msg_sender', 'msg_sender_email', 'msg', 'msg_sent']


class CVSer(serializers.ModelSerializer):
    class Meta:
        model = CV
        fields = ('name', 'surname', 'links', 'summary', 'experience', 'skills', 'qr')
        depth = 2


class LinksSer(serializers.ModelSerializer):
    parent = CVSer(many=False, read_only=True)
    class Meta:
        model = Links
        fields = ('parent', 'icon_name', 'link')


class ProfessionalSummarySer(serializers.ModelSerializer):
    parent = CVSer(many=False, read_only=True)
    class Meta:
        model = ProfessionalSummary
        fields = ('parent', 'summary_text')


class WorkExperienceSer(serializers.ModelSerializer):
    parent = CVSer(many=True, read_only=True)
    class Meta:
        model = WorkExperience
        fields = ('workplace_position', 'fromdate', 'todate', 'specificroute', 'responsibilities')

class WorkExperienceResponsibSer(serializers.ModelSerializer):

    class Meta:
        model = WorkExperienceResponsib
        fields = ('parent', 'responsibilities')


class SkillsSer(serializers.ModelSerializer):
    parent = CVSer(many=False, read_only=True)
    class Meta:
        model = Skills
        fields = ('parent', 'main_skill', 'skill_details')


class QRCodeSer(serializers.ModelSerializer):
    parent = CVSer(many=False, read_only=True)
    class Meta:
        model = QRCode
        fields = ('parent', 'qr')
